package com.paic.arch.jmsbroker;

import java.util.Map;

/**
 * JmsTester
 *
 * @author bianrongjun
 * @version 1.0
 */
public class JmsMessageBrokerSupportTester implements MessageBrokerSupportTester{

	JmsMessageBrokerSupport JMS_SUPPORT;
	
	Map config=null;
	
	private static String REMOTE_BROKER_URL;
	
	public JmsMessageBrokerSupportTester(Map config){
		this.config=config;
	}
	
	/**
	 * @see com.paic.arch.jmsbroker.MessageBrokerSupportTester#setup()
	 */
	@Override
	public void setup() throws Exception {
		JMS_SUPPORT 		= JmsMessageBrokerSupport.createARunningEmbeddedBrokerOnAvailablePort();
		REMOTE_BROKER_URL 	= JMS_SUPPORT.getBrokerUrl();
	}

	/**
	 * @see com.paic.arch.jmsbroker.MessageBrokerSupportTester#teardown()
	 */
	@Override
	public void teardown() throws Exception {
		JMS_SUPPORT.stopTheRunningBroker();
	}

	/**
	 * @see com.paic.arch.jmsbroker.MessageBrokerSupportTester#sendsMessagesToTheRunningBroker()
	 */
	@Override
	public Long sendsMessagesToTheRunningBroker() throws Exception {
		JmsMessageBrokerSupport
			.bindToBrokerAtUrl(REMOTE_BROKER_URL)
			.andThen()
			.sendATextMessageToDestinationAt((String)config.get("TEST_QUEUE"), (String)config.get("MESSAGE_CONTENT"));
		return JMS_SUPPORT.getEnqueuedMessageCountAt((String)config.get("TEST_QUEUE"));
	}

	/**
	 * @see com.paic.arch.jmsbroker.MessageBrokerSupportTester#readsMessagesPreviouslyWrittenToAQueue()
	 */
	@Override
	public String readsMessagesPreviouslyWrittenToAQueue() throws Exception {
		// TODO Auto-generated method stub
		return JmsMessageBrokerSupport
        		.bindToBrokerAtUrl(REMOTE_BROKER_URL)
                .sendATextMessageToDestinationAt((String)config.get("TEST_QUEUE"), (String)config.get("MESSAGE_CONTENT"))
                .andThen()
                .retrieveASingleMessageFromTheDestination((String)config.get("TEST_QUEUE"));
	}

	/**
	 * @see com.paic.arch.jmsbroker.MessageBrokerSupportTester#throwsExceptionWhenNoMessagesReceivedInTimeout()
	 */
	@Override
	public void throwsExceptionWhenNoMessagesReceivedInTimeout() throws Exception {
		JmsMessageBrokerSupport.bindToBrokerAtUrl(REMOTE_BROKER_URL).retrieveASingleMessageFromTheDestination((String)config.get("TEST_QUEUE"), 1);
	}

	/**
	 * @see com.paic.arch.jmsbroker.MessageBrokerSupportTester#getType_throwsExceptionWhenNoMessagesReceivedInTimeout()
	 */
	@Override
	public Class getType_throwsExceptionWhenNoMessagesReceivedInTimeout() {
		return JmsMessageBrokerSupport.NoMessageReceivedException.class;
	}

}
