package com.paic.arch.jmsbroker;

/**
 * 测试器
 *
 * @author bianrongjun
 * @version 1.0
 */
public interface MessageBrokerSupportTester {

	/** 
	 * setup
	 * @throws Exception
	 */
	public void setup() 											throws Exception;

	/** 
	 * teardown
	 * @throws Exception
	 */
	public void teardown() 											throws Exception ;

	/** 
	 * sendsMessagesToTheRunningBroker
	 * @return
	 * @throws Exception
	 */
	public Long sendsMessagesToTheRunningBroker() 				throws Exception;

	/** 
	 * readsMessagesPreviouslyWrittenToAQueue
	 * @return
	 * @throws Exception
	 */
	public String readsMessagesPreviouslyWrittenToAQueue() 			throws Exception;

	/** 
	 * throwsExceptionWhenNoMessagesReceivedInTimeout
	 * @throws Exception
	 */
	public void throwsExceptionWhenNoMessagesReceivedInTimeout() 	throws Exception ;
	
	/** 
	 * getType_throwsExceptionWhenNoMessagesReceivedInTimeout
	 * @return
	 */
	public Class getType_throwsExceptionWhenNoMessagesReceivedInTimeout();
	
}
