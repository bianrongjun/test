package com.paic.arch.jmsbroker;

import static org.assertj.core.api.Assertions.assertThat;

import java.util.HashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;

import org.junit.AfterClass;
import org.junit.BeforeClass;
import org.junit.Test;

/**
 * JmsMessageBrokerSupportTest
 *
 * @author bianrongjun
 * @version 1.0
 */
public class JmsMessageBrokerSupportTest {

    public static final String TEST_QUEUE 					= "MY_TEST_QUEUE";
    public static final String MESSAGE_CONTENT 				= "Lorem blah blah";
    
    private static Map config								=new HashMap();
    private static List<MessageBrokerSupportTester> MBSTs	=new LinkedList();
    
    @BeforeClass
    public static void setup() throws Exception {
    	
    	//配置
    	config.put("TEST_QUEUE"			, TEST_QUEUE);
    	config.put("MESSAGE_CONTENT"	, MESSAGE_CONTENT);
    	
    	//添加测试器(也可Spring扫描，或者读取配置文件方式获取类或实例来创建)
    	MBSTs.add(new JmsMessageBrokerSupportTester(config));
//    	JMBSTs.add(new IbmMqTester(config));
//    	JMBSTs.add(new TibcoMqTester(config));
    	
    	for(MessageBrokerSupportTester jmbst:MBSTs){
    		jmbst.setup();
		}
    }

    @AfterClass
    public static void teardown() throws Exception {
    	for(MessageBrokerSupportTester jmbst:MBSTs){
    		jmbst.teardown();
		}
    }

    @Test
    public void sendsMessagesToTheRunningBroker() throws Exception {
    	for(MessageBrokerSupportTester jmbst:MBSTs){
    		assertThat(jmbst.sendsMessagesToTheRunningBroker()).isEqualTo(1);;
		}
    }

    @Test
    public void readsMessagesPreviouslyWrittenToAQueue() throws Exception {
    	for(MessageBrokerSupportTester jmbst:MBSTs){
    		assertThat(jmbst.readsMessagesPreviouslyWrittenToAQueue()).isEqualTo(MESSAGE_CONTENT);;
		}
    }

    @Test
    public void throwsExceptionWhenNoMessagesReceivedInTimeout() throws Exception {
    	for(MessageBrokerSupportTester jmbst:MBSTs){
    		try {
    			jmbst.throwsExceptionWhenNoMessagesReceivedInTimeout();
			} catch (Exception e) {
				assertThat(e.getClass()).isEqualTo(jmbst.getType_throwsExceptionWhenNoMessagesReceivedInTimeout());
			}
		}
    }


}