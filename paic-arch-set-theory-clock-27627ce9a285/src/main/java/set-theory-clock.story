When the time is 00:00:00
Then the clock should look like B|BBBB|BBBB|BBBBBBBBBBB|BBBB
When the time is 09:33:24
Then the clock should look like B|RBBB|RRRR|YYRYYRBBBBB|YYYB
When the time is 23:59:59
Then the clock should look like R|RRRR|RRRB|YYRYYRYYRYY|YYYY