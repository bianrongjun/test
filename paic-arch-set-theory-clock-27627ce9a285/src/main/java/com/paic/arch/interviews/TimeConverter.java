package com.paic.arch.interviews;

public class TimeConverter {
	
	/**
	 * ClockPart
	 */
	class ClockPart{
		
		/** 时间跨度（单位秒） */
		int timeunit;
		/** 刻度最大数量 */
		int maxlen;
		/** 使用相反色(黄<>红) */
		boolean oppsite;
		/** 间隔色跨度 */
		Integer interval;
		
		/**
		 * 
		 * @param timeunit
		 * @param maxlen
		 */
		public ClockPart(int timeunit,int maxlen,boolean oppsite,Integer interval){
			this.timeunit	=timeunit;
			this.maxlen		=maxlen;
			this.oppsite	=oppsite;
			this.interval	=interval;
		}
		
		public String get(int time){
			char main_color		=oppsite?'Y':'R';
			char interval_color	=oppsite?'R':'Y';
			
			int s				=(time/timeunit)%maxlen;
			StringBuffer sb		=new StringBuffer();
			for(int i=0;i<maxlen-1;i++){
				if(i<s){
					sb.append(interval!=null&&(i%interval==(interval-1))?interval_color:main_color);
				}else{
					sb.append('B');
				}
			}
			return sb.toString();
		}
	}
	
    String convertTime(String aTime){

    	String[] ts	=	aTime.split(":");
    	int time	=	Integer.valueOf(ts[0])*60*60+
    					Integer.valueOf(ts[1])*60+
    					Integer.valueOf(ts[2]);
    	return 
    		new ClockPart(1			,2	,false	,null)	.get(time)+"|"+		//第一排
    		new ClockPart(5*60*60	,5	,false	,null)	.get(time)+"|"+		//第二排
    		new ClockPart(1*60*60	,5	,false	,null)	.get(time)+"|"+		//第三排
    		new ClockPart(5*60		,12	,true	,3)		.get(time)+"|"+		//第四排
    		new ClockPart(60		,5	,true	,null)	.get(time)			//第五排
    	;
    }
    
    public static void main(String[] args) {
    	for (int i = 0; i < 24; i++) {
    		for (int j = 0; j < 60; j++) {
    			for (int k = 0; k < 60; k++) {
    				String time=
    					(i<10?"0":"")+i+":"+
    					(j<10?"0":"")+j+":"+
    					(k<10?"0":"")+k;
    				System.out.println("["+time+"]"+new TimeConverter().convertTime(time));//"01:59:22"
    			}
    		}
		}
		
	}
    
}
